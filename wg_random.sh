#!/bin/bash

rm /etc/wg0.conf 
# dir with wireguard config files
CONFIG_DIR="/etc/wireguard"

# load the conf files in an array
CONFIG_FILES=()

# add each file to that array
for file in "$CONFIG_DIR"/*; do
    if [ -f "$file" ]; then
        CONFIG_FILES+=("$file")
    fi
done

# check if there is any file to use or exit
if [ ${#CONFIG_FILES[@]} -eq 0 ]; then
    echo "Keine WireGuard-Konfigurationsdateien gefunden."
    exit 1
fi

# pick random conf file
RANDOM_INDEX=$(( RANDOM % ${#CONFIG_FILES[@]} ))
SELECTED_CONFIG="${CONFIG_FILES[$RANDOM_INDEX]}"
# connect to random exit node from pool
sudo wg-quick up "$SELECTED_CONFIG"
iptables -I OUTPUT ! -o wg0 -m mark ! --mark $(wg show wg0 fwmark) -m addrtype ! --dst-type LOCAL -j REJECT
ln -s /tmp/wg0.conf /etc/wireguard/wg0.conf
echo "WireGuard connected to $SELECTED_CONFIG"

